var searchData=
[
  ['getcurrentfontheight',['getCurrentFontHeight',['../classev3api_1_1_l_c_d.html#a5d0b51b5f5c4ad1ca052059df3f9db6a',1,'ev3api::LCD']]],
  ['getcurrentfontwidth',['getCurrentFontWidth',['../classev3api_1_1_l_c_d.html#a07abc38fff7e9d05ce1668b762799d03',1,'ev3api::LCD']]],
  ['getcurrentma',['getCurrentmA',['../classev3api_1_1_e_v3ext.html#ab871115663e86ee75ed28a00df878bb2',1,'ev3api::EV3ext']]],
  ['getcurrentmv',['getCurrentmV',['../classev3api_1_1_e_v3ext.html#ad679cbe78384603b12675cce56b8f7b8',1,'ev3api::EV3ext']]],
  ['getfontsize',['getFontSize',['../classev3api_1_1_l_c_d.html#a02170469c3df52bb845d072ed8fcd14a',1,'ev3api::LCD']]],
  ['getfpbt',['getFpBT',['../classev3api_1_1_bluetooth.html#a886a3beb09e82f1862b8968a519ae844',1,'ev3api::Bluetooth']]],
  ['getheight',['getHeight',['../classev3api_1_1_l_c_d.html#a70df3bb2b4b3e4b2acb22763aebf120e',1,'ev3api::LCD']]],
  ['gettnum',['getTNum',['../classev3api_1_1_button.html#aa527f2521feeb3f7e619949e8cf21788',1,'ev3api::Button']]],
  ['getwidth',['getWidth',['../classev3api_1_1_l_c_d.html#a6adce1b54ceb329c32b81fd7fe8082bb',1,'ev3api::LCD']]]
];
