/*
 * EV3ext.h
 *
 *  Created on: 2016/02/06
 *      Author: yasuh
 */

#ifndef EV3EXT_H_
#define EV3EXT_H_

#include "ev3api.h"

namespace ev3api {

class EV3ext {

public:
	explicit EV3ext();
	~EV3ext();

public:

	/**
	 * @brief         バッテリの電流を取得する．
	 * @returns       バッテリの電流（mA）
	 */
	int getCurrentmA();

	/**
	 * @brief         バッテリの電圧を取得する．
	 * @returns       バッテリの電圧（mV）
	 */
	int getCurrentmV();

	/**
	 * @brief 		 LEDライトのカラーを設定する
	 * @details      不正の設定値を指定した場合，LEDライトのカラーを変えない．
	 * @param  color LEDカラーの設定値
	 * @retval E_OK  正常終了
	 * @retval E_PAR 不正の設定値
	 */
	ER setLEDColor(ledcolor_t color);
};

}

#endif /* EV3EXT_H_ */
