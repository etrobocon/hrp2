/*
 * Button.h
 *
 *  Created on: 2016/02/06
 *      Author: yasuh
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "ev3api.h"

namespace ev3api {

class Button {

private:
	button_t button = LEFT_BUTTON;

public:
	explicit Button(button_t button);
	virtual ~Button();

public:
	/**
	 * @brief ボタンの総数を返す
	 * @return ボタン総数
	 */
	uint16_t getTNum();

public:

	/**
	 * @brief         ボタンの押下状態を取得する．
	 * @details       不正のボタン番号を指定した場合，常に \a false を返す（エラーログが出力される）．
	 * @retval true   押されている状態
	 * @retval false  押されていない状態
	 */
	virtual bool_t isPressed();

	/**
	 * @brief          指定したボタンのクリックイベントハンドラを設定する．
	 * @details        ボタンハンドラはタスクコンテストで実行する．デフォルトは，待ち禁止状態から呼び出される．
	 * @param  handler イベントハンドラ．NULLを指定した場合，元のハンドラがクリアされる
	 * @param  exinf   イベントハンドラの拡張情報
	 * @retval E_OK    正常終了
	 * @retval E_ID    不正のボタン番号
	 */
	virtual ER setOnClicked(ISR handler, intptr_t exinf);


	/**
	 * @brief ボタンの名前を返す
	 * @return ボタンの名前の文字列
	 * @retval LEFT_BUTTON
	 * @retval RIGHT_BUTTON
	 * @retval UP_BUTTON
	 * @retval DOWN_BUTTON
	 * @retval ENTER_BUTTON
	 * @retval BACK_BUTTON
	 */
	const char* toString();
};

}

#endif /* BUTTON_H_ */
