/*
 * Speaker.h
 *
 *  Created on: 2016/02/06
 *      Author: yasuh
 */

#ifndef SPEAKER_H_
#define SPEAKER_H_

#include "ev3api.h"

namespace ev3api {

class Speaker {

public:
	explicit Speaker();
	~Speaker();

public:

	/**
	 * @brief          音量を調整する．
	 * @param  volume  ボリュームの値．範囲：0から+100．0はミュート．+100を超えた値を指定すると，実際の値は+100になる．
	 * @retval E_OK    正常終了
	 * @retval E_CTX   非タスクコンテストから呼び出す
	 * @retval E_NORES サウンドデバイスが占有されている
	 */
	ER setVolume(uint8_t volume);

	/**
	 * @brief           指定した周波数でトーン出力する．今再生しているサウンドは停止される．
	 * @param frequency トーンの周波数
	 * @param duration  出力持続時間．単位：ミリ秒．SOUND_MANUAL_STOPを指定した場合は手動で停止する．
	 * @retval E_OK     正常終了
	 * @retval E_CTX    非タスクコンテストから呼び出す
	 * @retval E_NORES  サウンドデバイスが占有されている
	 */
	ER playTone(uint16_t frequency, int32_t duration);

	/**
	 * @brief            指定したWAVファイル（8-bit 8kHz mono）を再生する．今再生しているサウンドは停止される．
	 * @param  memfile WAVファイルのメモリファイルへのポインタ
	 * @param  duration  出力持続時間．単位：ミリ秒．SOUND_MANUAL_STOPを指定した場合は手動で停止しないと最後まで再生する．
	 * @retval E_OK      正常終了
	 * @retval E_CTX     非タスクコンテストから呼び出す
	 * @retval E_NORES   サウンドデバイスが占有されている
	 */
	ER playFile(const memfile_t *memfile, int32_t duration);

	/**
	 * @brief            今再生しているサウンドを停止する．
	 * @retval E_OK      正常終了
	 * @retval E_CTX     非タスクコンテストから呼び出す
	 * @retval E_NORES   サウンドデバイスが占有されている
	 */
	ER stop();
};

}

#endif /* SPEAKER_H_ */
