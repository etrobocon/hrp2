/*
 * Bluetooth.h
 *
 *  Created on: 2016/02/06
 *      Author: yasuh
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "ev3api.h"

namespace ev3api {

class Bluetooth {

private:
	FILE* fpBT;

public:
	explicit Bluetooth();
	virtual ~Bluetooth();

public:

	/**
	 * @brief            Bluetooth (Serial Port Profile)が接続されているかどうかをチェックする．
	 * @retval true      接続済み．Bluetooth仮想シリアルポートで通信できる．
	 * @retval false     接続切れ．
	 */
	bool_t isConnected();

	/**
	 * @brief Bluetooth経由で1行送信するメソッド
	 * @param msg 送信文字列
	 */
	void send(const char* msg);

	/**
	 * @brief Bluetooth経由で1行受信するメソッド
	 * @param buf	受信バッファ
	 * @param size	受信バッファのサイズ
	 * @return 受信文字数
	 */
	int16_t recv(char* buf, int16_t size);

	/**
	 * @brief Bluetooth接続のファイルポインタを取得するメソッド
	 * @return Bluetooth接続のファイルポインタ
	 */
	FILE* getFpBT();

	/**
	 * @brief Bluetooth経由で1文字送信するメソッド
	 * @param c	送信文字
	 * @return 書き込まれた文字あるいはエラー時はEOF
	 */
	int btputc(char c);

	/**
	 *　@brief Bluetooth経由で1文字受信するメソッド
	 *　@return 受信した文字
	 */
	char btgetc();
};

}

#endif /* BLUETOOTH_H_ */
