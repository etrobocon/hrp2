/*
 * LCD.h
 *
 *  Created on: 2016/02/06
 *      Author: yasuh
 */

#ifndef LCD_H_
#define LCD_H_

#include "ev3api.h"

namespace ev3api {

class LCD {

private:
	static lcdfont_t currentFont;

public:
	// コンストラクタ
	explicit LCD();

	// デストラクタ
	~LCD();

public:
	// wrapped LCD function in EV3RT C API

	/**
	 * @brief           フォントのサイズを取得する．
	 * @param  font     フォントの番号
	 * @param  width  取得したフォントの幅を格納する場所へのポインタ．NULLの場合は格納しない．
	 * @param  height 取得したフォントの高さを格納する場所へのポインタ．NULLの場合は格納しない．
	 * @retval E_OK     正常終了
	 * @retval E_ID     不正ID番号
	 */
	ER getFontSize(lcdfont_t font, int32_t * width, int32_t * height);

	/**
	 * @brief	メモリイメージを指定座標に描画するメソッド
	 * @param image	描画対象のメモリイメージ
	 * @param x	描画開始位置のX座標
	 * @param y	描画開始位置のY座標
	 * @return	常にE_OK
	 */
	ER drawImage(const image_t * image, int32_t x, int32_t y);

	/**
	 * @brief             指定座標で線を引く．
	 * @param  x0         始点の水平方向の位置
	 * @param  y0         始点の垂直方向の位置
	 * @param  x1         終点の水平方向の位置
	 * @param  y1         終点の垂直方向の位置
	 * @retval E_OK       正常終了
	 */
	ER drawLine(int32_t x0, int32_t y0, int32_t x1, int32_t y1);

	/**
	 * @brief        デフォルトのフォントを設定する．
	 * @param  font  フォントの番号
	 * @retval E_OK  正常終了
	 * @retval E_ID  不正ID番号
	 */
	ER setFont(lcdfont_t font);

	/**
	 * @brief 現在指定されているフォントの高さを取得するメソッド
	 * @return フォントの高さ
	 */
	int32_t getCurrentFontHeight();

	/**
	 * @brief 現在していされているフォントの幅を取得するメソッド
	 * @return フォントの幅
	 */
	int32_t getCurrentFontWidth();

	/**
	 * @brief	指定座標に文字列を出力するメソッド
	 * @details	指定座標以降を出力前に空白で初期化する（autoClear: trueの場合）
	 * @param str	出力文字列
	 * @param x	左上隅のX座標
	 * @param y	左上隅のY座標
	 * @param autoClear	行を事前にクリアする(true)か、否(false)か
	 * @return	常にE_OK
	 */
	ER drawString(const char * str, int32_t x, int32_t y, bool_t autoClear = true);

	/**
	 * @brief            矩形を描いて色を塗る．
	 * @param  x         左上隅の水平方向の位置
	 * @param  y         左上隅の垂直方向の位置
	 * @param  w         矩形の幅
	 * @param  h         矩形の高さ
	 * @param  color     カラー
	 * @retval E_OK      正常終了
	 */
	ER fillRect(int32_t x, int32_t y, int32_t w, int32_t h, lcdcolor_t color);

	/**
	 * @brief            メモリファイルから画像をロードする．
	 * @details          指定したメモリファイルから画像のオブジェクトを生成する．現時点では，BMP形式のモノクロ画像ファイルしかサポートしない．
	 *                   エラーが発生する場合，\a image の \a data はNULLにクリアする.
	 * @param  memfile 画像ファイルが格納されるメモリファイルのオブジェクト
	 * @param  image   生成した画像のオブジェクトを格納する場所へのポインタ
	 * @retval E_OK      正常終了
	 * @retval E_NOMEM   メモリ不足
	 * @retval E_NOSPT   画像ファイルの形式は未対応
	 * @retval E_OBJ     画像ファイルは破損
	 * @retval E_PAR     メモリファイルは無効
	 */
	ER loadImage(const memfile_t * memfile, image_t * image);

	/**
	 * @brief            画像のオブジェクトを解放する．
	 * @details          画像のオブジェクトにより確保されたリソース（メモリ領域）を解放する．正常終了の場合，\a image の \a data はNULLにクリアする．
	 * @param  image   解放する画像のオブジェクトのポインタ
	 * @retval E_OK      正常終了
	 * @retval E_PAR     \a image はNULL
	 */
	ER freeImage(image_t * image);

	/**
	 * @brief LCDの描画領域全体をクリアするメソッド
	 */
	void clear();

	/**
	 * @brief LCDの描画領域の高さを取得するメソッド
	 * @return LCDの描画領域の高さ
	 */
	uint16_t getHeight();

	/**
	 * @brief LCDの描画領域の幅を取得するメソッド
	 * @return LCDの描画領域の幅
	 */
	uint16_t getWidth();
};

}

#endif /* LCD_H_ */
