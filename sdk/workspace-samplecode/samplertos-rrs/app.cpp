/**
 * RTOS Task Sample Program
 *
 * This program shows "Hello Task1", "Hello Task2" and "Hello Task 3" in rotation.
 * These 3 tasks have same priority, and they do not use sleep.
 * If TOPPERS/HRRP2 supports Round Robin Scheduling, 3 tasks run in rotation.
 * If no, only 1 task run, and others stay on ready status.
 *
 * Written by Yasuhiro Noguchi
 * Email: yasuhiro.noguchi@gmail.com
 */

#include "ev3api.h"
#include "app.h"

// 利用するAPIのヘッダファイル
#include "LCD.h"
#include "Clock.h"

// EV3APIの名前空間
using namespace ev3api;

// EV3APIのオブジェクト
auto clock = new Clock();
auto lcd = new LCD();

void wait(uint32_t duration) {
	uint32_t base = clock->now();
	while ( base + duration > clock->now() );
	return;
}

// TASK defined in app.cfg
void user_task1(intptr_t unused) {
	lcd->setFont(EV3_FONT_MEDIUM);
	char str[32] = "";
	int16_t n = 0;
	while (true) {
		snprintf(str, 32, "USER_TASK1: %d", n++);
		lcd->drawString(str, 0, lcd->getCurrentFontHeight());
		wait(1000);
	}
}

// TASK defined in app.cfg
void user_task2(intptr_t unused) {
	lcd->setFont(EV3_FONT_MEDIUM);
	char str[32] = "";
	int16_t n = 0;
	while (true) {
		snprintf(str, 32, "USER_TASK2: %d", n++);
		lcd->drawString(str, 0, lcd->getCurrentFontHeight()*2);
		wait(1000);
	}
}

// TASK defined in app.cfg
void user_task3(intptr_t unused) {
	lcd->setFont(EV3_FONT_MEDIUM);
	char str[32] = "";
	int16_t n = 0;
	while (true) {
		snprintf(str, 32, "USER_TASK3: %d", n++);
		lcd->drawString(str, 0, lcd->getCurrentFontHeight()*3);
		wait(1000);
	}
}
