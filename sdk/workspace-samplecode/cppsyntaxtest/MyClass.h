/*
 * MyClass.h
 *
 *  Created on: 2016/04/20
 *      Author: yasuh
 */

#ifndef SAMPLECLASS_MYCLASS_H_
#define SAMPLECLASS_MYCLASS_H_

#include "SuperClass.h"
#include "LCD.h"

using namespace ev3api;

class MyClass : public SuperClass {

private:
	LCD lcd_instance_value;
	LCD* lcd_p;
	LCD& lcd_r;

public:
	MyClass(LCD* lcd_p, LCD& lcd_r);
	virtual ~MyClass();

public:
	virtual void showHelloMyClass();
};

#endif /* SAMPLECLASS_MYCLASS_H_ */
