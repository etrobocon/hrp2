/*
 * SuperClass.cpp
 *
 *  Created on: 2016/04/20
 *      Author: yasuh
 */
#include "SuperClass.h"

SuperClass::SuperClass() {
}

SuperClass::~SuperClass() {
}

void SuperClass::greeting() {
	showHelloMyClass();
}

void SuperClass::showHelloMyClass() {
	lcd_super.drawString("method in super class", 0, lcd_super.getCurrentFontHeight()*1);
}
