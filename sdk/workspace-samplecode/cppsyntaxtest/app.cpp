/**
 * Using C++ Class Sample Program
 *
 * Written by Yasuhiro Noguchi
 * Email: yasuhiro.noguchi@gmail.com
 */

#include "ev3api.h"
#include "app.h"

#include "MyClass.h"

// 利用するAPIのヘッダファイル
#include "LCD.h"
#include "Clock.h"

// EV3APIの名前空間
using namespace ev3api;

// EV3APIのオブジェクト
auto clock = new Clock();
auto lcd = new LCD();

auto mcg = new MyClass(lcd, *lcd);

void wait(uint32_t duration) {
	uint32_t base = clock->now();
	while ( base + duration > clock->now() );
	return;
}

// MAIN_TASK1 defined in app.cfg
void main_task(intptr_t unused) {
	lcd->setFont(EV3_FONT_MEDIUM);

	// use class as local value
	lcd->clear();
	lcd->drawString("MyClass(L):", 0,0);
	MyClass mc(lcd, *lcd);
	mc.showHelloMyClass();
	clock->wait(3000);

	// use class as global value
	lcd->clear();
	lcd->drawString("MyClass(G):", 0,0);
	mcg->showHelloMyClass();
	clock->wait(3000);

	// use Polymorphism
	lcd->clear();
	lcd->drawString("SuperClass:", 0,0);
	SuperClass sc;
	sc.greeting();
	clock->wait(3000);

	lcd->clear();
	lcd->drawString("psc->showHelloMyClass", 0,0);
	SuperClass* psc = mcg;
	psc->showHelloMyClass();
	clock->wait(3000);

	lcd->clear();
	lcd->drawString("pol_sc->greeting", 0,0);
	psc->greeting();
	clock->wait(3000);
}
