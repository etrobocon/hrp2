/*
 * MyClass.cpp
 *
 *  Created on: 2016/04/20
 *      Author: yasuh
 */
#include "MyClass.h"

auto lcd_auto = new LCD();

MyClass::MyClass(LCD* lcd_p, LCD& lcd_r)
	: lcd_r(this->lcd_r){
	this->lcd_p = lcd_p;
}

MyClass::~MyClass() {
}

void MyClass::showHelloMyClass() {
	LCD lcd;
	lcd.setFont(EV3_FONT_MEDIUM);
	lcd.drawString("inst value", 0, lcd.getCurrentFontHeight());
	lcd_auto->drawString("global with auto", 0, lcd_auto->getCurrentFontHeight()*2);
	lcd_p->drawString("inst value pointer", 0, lcd_auto->getCurrentFontHeight()*3);
	lcd_r.drawString("inst value ref", 0, lcd_auto->getCurrentFontHeight()*4);
	SuperClass::lcd_super.drawString("super inst value", 0, lcd_auto->getCurrentFontHeight()*5);
}
