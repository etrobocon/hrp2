/*
 * SuperClass.h
 *
 *  Created on: 2016/04/20
 *      Author: yasuh
 */

#ifndef SAMPLECLASS_SUPERCLASS_H_
#define SAMPLECLASS_SUPERCLASS_H_

#include "LCD.h"

using namespace ev3api;

class SuperClass {
protected:
	LCD lcd_super;

public:
	SuperClass();
	virtual ~SuperClass();

public:
	virtual void greeting();
	virtual void showHelloMyClass();
};

#endif /* SAMPLECLASS_SUPERCLASS_H_ */
