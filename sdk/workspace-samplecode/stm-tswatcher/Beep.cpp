/*
 * Beep.cpp
 *
 *  Created on: 2016/04/21
 *      Author: yasuh
 */
#include "Beep.h"

Beep::Beep() {
}

Beep::~Beep() {
}

void Beep::execute() {
	mSpeaker.playTone(300, 1000);
}
