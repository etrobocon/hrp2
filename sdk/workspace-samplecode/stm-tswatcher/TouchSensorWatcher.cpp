#include "TouchSensorWatcher.h"

TouchSensorWatcher::TouchSensorWatcher(TouchSensor& ts, EventListener& evl):
	mTs(ts),
	mEvl(evl){
}

TouchSensorWatcher::~TouchSensorWatcher() {
}

bool TouchSensorWatcher::isPressed() {
	return mTs.isPressed();
}

bool TouchSensorWatcher::isNotPressed() {
	return !isPressed();
}

void TouchSensorWatcher::beep() {
	mEvl.execute();
}
