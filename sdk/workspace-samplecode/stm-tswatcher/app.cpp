/**
 *
 * TouchSensorWatcher sample program generated from State Machine Model.
 *
 */

#include "TouchSensorWatcher.h"
#include "Beep.h"
#include "ev3api.h"
#include "app.h"

#include "LCD.h"
#include "Clock.h"

using namespace ev3api;

// EV3APIオブジェクト
auto clock = new Clock();
auto lcd = new LCD();

// タッチセンサーオブジェクトを生成
auto ts = new TouchSensor(PORT_1);
auto beep = new Beep();
auto tsw = new TouchSensorWatcher(*ts, *beep);
TouchSensorWatcherSTM* tswstm = tsw;

// タッチセンサ監視状態遷移モデルの駆動タスク（周期ハンドラから起動）
void tswatcher_task(intptr_t unused) {
	tswstm->step();
}

// タッチセンサ監視用の周期ハンドラ
void tswatcher_cyc_handler(intptr_t unused) {
	// 周期ハンドラの占有CPU時間を最小にするため最低限の処理に限定
	act_tsk(TSWATCHER_TASK);
}
