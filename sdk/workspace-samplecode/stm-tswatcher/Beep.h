/*
 * Beep.h
 *
 *  Created on: 2016/04/21
 *      Author: yasuh
 */

#ifndef STM_TSWATCHER_BEEP_H_
#define STM_TSWATCHER_BEEP_H_

#include "EventListener.h"
#include "Speaker.h"

using namespace ev3api;

class Beep : public EventListener {
private:
	Speaker mSpeaker;

public:
	Beep();
	virtual ~Beep();

public:
	void execute();
};

#endif /* STM_TSWATCHER_BEEP_H_ */
