#ifndef TOUCHSENSORWATCHER_H
#define TOUCHSENSORWATCHER_H

#include "TouchSensorWatcherSTM.h"
#include "EventListener.h"
#include "TouchSensor.h"

using namespace ev3api;

class TouchSensorWatcher: public TouchSensorWatcherSTM {

private:
	TouchSensor& mTs;
	EventListener& mEvl;

public:
	virtual void beep();
	virtual bool isNotPressed();
	virtual bool isPressed();
	TouchSensorWatcher(TouchSensor& ts, EventListener& evl);
	virtual ~TouchSensorWatcher();
};

#endif /*TOUCHSENSORWATCHER_H*/

