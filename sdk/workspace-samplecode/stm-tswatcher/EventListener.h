/*
 * EventListener.h
 *
 *  Created on: 2016/04/21
 *      Author: yasuh
 */

#ifndef STM_TSWATCHER_EVENTLISTENER_H_
#define STM_TSWATCHER_EVENTLISTENER_H_

/**
 * イベントリスナインタフェースクラス
 */
class EventListener {
public:
	virtual ~EventListener();
public:
	virtual void execute();
};

inline EventListener::~EventListener() {
}

inline void EventListener::execute() {
}

#endif /* STM_TSWATCHER_EVENTLISTENER_H_ */
