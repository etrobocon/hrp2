/**
 * DriverController sample program generated from State Machine Model.
 *
 *　イベントごとに停止状態 -> 前進状態 -> 後退状態 (-> 停止状態) の3状態間を遷移し、モーターを制御する状態遷移モデル
 * このプロジェクトではテスト用に3secごとに合計6回のイベントを発行するタスクを設定している
 */

#include "DriveController.h"
#include "ev3api.h"
#include "app.h"

#include "LCD.h"
#include "Clock.h"
#include "Motor.h"
#include "../stm-tswatcher/EventListener.h"

using namespace ev3api;

// EV3APIオブジェクト
auto clock = new Clock();
auto lcd = new LCD();
auto motorR = new Motor(PORT_B);
auto motorL = new Motor(PORT_C);

// ドライブコントローラオブジェクトを生成
auto dc = new DriveController(*motorL, *motorR);
EventListener* evl = dc;

// ドライブコントローラのテスト用タスク
void drivectl_task(intptr_t exinf) {

	// EventListenerインタフェースを介してアクセス
	lcd->drawString("ST -> MF", 0, 0);
	evl->execute();
	clock->sleep(3000);
	lcd->drawString("MF -> MB", 0, 0);
	evl->execute();
	clock->sleep(3000);
	lcd->drawString("MB -> ST", 0, 0);
	evl->execute();
	clock->sleep(3000);
	lcd->drawString("ST -> MF", 0, 0);
	evl->execute();
	clock->sleep(3000);
	lcd->drawString("MF -> MB", 0, 0);
	evl->execute();
	clock->sleep(3000);
	lcd->drawString("MB -> ST", 0, 0);
	evl->execute();
}
