#ifndef DRIVECONTROLLER_H
#define DRIVECONTROLLER_H

#include "DriveControllerSTM.h"
#include "Motor.h"
#include "../stm-tswatcher/EventListener.h"

using namespace ev3api;

class DriveController: public DriveControllerSTM, public EventListener {

private:
	Motor& mMotorL;
	Motor& mMotorR;
	int32_t bPWM = 30;

public:
	virtual void mBackward();
	virtual void stop();
	virtual void mForward();
	DriveController(Motor& motorL, Motor& motorR);
	virtual ~DriveController();

public:
	void execute();
};

#endif /*DRIVECONTROLLER_H*/

