#include "DriveController.h"

DriveController::DriveController(Motor& motorL, Motor& motorR) :
		mMotorL(motorL), mMotorR(motorR) {
}

DriveController::~DriveController() {
}

void DriveController::mForward() {
	mMotorL.setPWM(bPWM);
	mMotorR.setPWM(bPWM);
}

void DriveController::stop() {
	mMotorL.setPWM(0);
	mMotorR.setPWM(0);
}

void DriveController::mBackward() {
	mMotorL.setPWM((-1) * bPWM);
	mMotorR.setPWM((-1) * bPWM);
}

void DriveController::execute() {
	next();
}

