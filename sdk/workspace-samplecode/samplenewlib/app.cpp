/**
 * Newlib Sample Program
 *   Test: snprintf and round (in math.h)
 *
 * Written by Yasuhiro Noguchi
 * Email: yasuhiro.noguchi@gmail.com
 */

#include "ev3api.h"
#include "app.h"
#include <math.h>

// 利用するAPIのヘッダファイル
#include "Clock.h"
#include "LCD.h"

// EV3APIの名前空間
using namespace ev3api;

// EV3APIのオブジェクト
auto clock = new Clock();
auto lcd = new LCD();

// MAIN_TASK defined in app.cfg
void main_task(intptr_t unused) {
	lcd->setFont(EV3_FONT_MEDIUM);
	char str[32] = "";
	snprintf(str, 32, "%lf", round(3.14));
	lcd->drawString(str, 0, 0);
}
