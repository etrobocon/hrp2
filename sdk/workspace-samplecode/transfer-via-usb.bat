::
:: Transfer app to EV3RT via USB Connection
::

::
:: Before run this script:
::  - Setup Environment Valuable: DSTDIR
::  - Connect EV3 via USB
::
@echo off
setlocal
cd /d %~dp0

set DSTDIR=D:\ev3rt\apps
set SRCDIR=.
set APPNAME=app
set APPFILE=%SRCDIR%\%APPNAME%

:: Check EV3 was connected.
if not exist "%DSTDIR%" (
	echo Error: Directory not found: %DSTDIR%
	pause
	exit
)

if not exist %APPFILE% (
	echo Error: App file not found: %APPFILE%
	pause
	exit
)

@copy /B /Y %APPNAME% %DSTDIR%
@fc /B %APPNAME% %DSTDIR%\%APPNAME%
@echo Transfer Finished!!

pause

