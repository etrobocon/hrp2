#pragma once
extern ID _module_id_APP_INIT_TASK;
#define APP_INIT_TASK ((const ID)(_module_id_APP_INIT_TASK))

extern ID _module_id_MAIN_TASK;
#define MAIN_TASK ((const ID)(_module_id_MAIN_TASK))

extern ID _module_id_SEM1;
#define SEM1 ((const ID)(_module_id_SEM1))

extern ID _module_id_FLG1;
#define FLG1 ((const ID)(_module_id_FLG1))

extern ID _module_id_DTQ1;
#define DTQ1 ((const ID)(_module_id_DTQ1))

extern ID _module_id_PDQ1;
#define PDQ1 ((const ID)(_module_id_PDQ1))

extern ID _module_id_MTX1;
#define MTX1 ((const ID)(_module_id_MTX1))

